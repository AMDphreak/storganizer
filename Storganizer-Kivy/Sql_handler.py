# Place HTML request to BarcodeLookup.com and store the result into local
# database
# Updates database if any fields have changed
import sys
import mysql.connector 

class Sql_handler: # kind of like a connection object
    
    connection = None # class variable
    num_instances = 0
    
    def __init__(self):
        # Create the database connection ( a singleton )
        # Singletons are shared objects that the class has as an attribute
        # and which all instances can access
        if type(self).connection is None:
            try:
                print("Connecting to AAGoods Database...")
                config = {'user': 'Warehouse Worker',
                          'host': 'aagoodsdev.com',
                          'password': 'warehouse'}
                # establish a connection applicable to all instances of this class
                # Singleton class variable:
                type(self).connection = mysql.connector.connect(**config)
                type(self).num_instances = 1
                print("Connection established")
            except mysql.connector.Error as err:
                # handle errors if connection fails
                print("Connection failed:")
                if err.errno == ER_SERVER_OFFLINE_MODE:
                    print("Server offline")
                elif err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                    print("Username or password not valid")
                else:
                    print(err)
                    print("Internal error: please yell at your database administrator")
                type(self).connection = None

        # make a shortcut to the class's connection
        self.connection = type(self).connection
        # increment instance tracker
        type(self).num_instances = type(self).num_instances + 1
        # create a new instance cursor
        self.cursor = self.connection.cursor()
        return

    def __del__(self):
        # get rid of an instance, decrement the counter
        type(self).num_instances = type(self).num_instances - 1
        # check if all instances are gone
        if type(self).num_instances == 0:
            # if so, close the database connection
            try:
                type(self).connection.close()
                self.cursor.close()
            except mysql.connector.Error as err:
                print(err)
        else:
            # otherwise, just close the cursor
            self.cursor.close()
        return

    def get_bays(self, barcode):
        query = "SELECT * FROM inventory.bays WHERE Barcode=%s;"
        vars_tuple = (barcode,)
        self.cursor.execute(query, vars_tuple)
        return self.cursor.fetchall() 

    def get_products(self, barcode):
        select = ("SELECT * FROM `products`.`products`"
                "WHERE Barcode=%s;")
        self.cursor.execute(select, barcode)
        return self.cursor.fetchall()

    # example:  get_inventory("0000", "store 1")
    def get_inventory(self, barcode, store_ID=None):
        """
        Gets store's inventory for the product/s with the given barcode
        @param barcode the `Barcode` value in the database
        @param store_ID the `Store ID` value in the database
        @return results: list of tuples (product:ID, quantity:)
        """
        if store == None:
            query = ("SELECT * FROM `inventory`.`inventory` WHERE `Barcode`=%s;")
            vars_tuple = (barcode,)
            # Notice the %s where I need to insert a variable.
            # You should not use the .format() function on a query, because this
            # allows someone to attack your db with SQL injection.
            # You must pass variables as a tuple. The mysql.connector will
            # check the tuple for dangerous injection code.
            # Example tuples:
            #   (var1,)
            #   (var1, var2)
            # Example call:
            #   cursor.execute(query, vars_tuple)
        else:
            query = ("SELECT * FROM `inventory`.`inventory` WHERE `Barcode`=%s AND `Store ID`=%s;")
            vars_tuple = (barcode, store_ID)
        self.cursor.execute(query,vars_tuple)
        return self.cursor.fetchall()

    def add_inventory(self, barcode, storeID, quantity):
        query = ("INSERT INTO inventory.inventory ALL")
        return

    def show_inventory_tables(self):
        return show_tables("inventory","store%")

    def show_tables(self, database, table_string):
        self.cursor.execute("SHOW TABLES FROM {} LIKE '{}'".format(database, table_string))
        return self.cursor.fetchall()