# Author: Ryan Johnson

import time

# 3rd party libraries
import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

# BarcodeLookup.com Database API
api_key = None #grab from settings

# Local Database Details
username = None #grab from settings
password = None #grab from settings
server = None #grab from settings

class LoginScreen(GridLayout):

    def __init__(self, **kwargs):
        super(LoginScreen, self).__init__(**kwargs)
        self.cols = 2
        self.add_widget(Label(text='User Name'))
        self.username = TextInput(multiline=False)
        self.add_widget(self.username)
        self.add_widget(Label(text='password'))
        self.password = TextInput(password=True, multiline=False)
        self.add_widget(self.password)

class Storganizer(App):
    version = 0.1
    def build(self):
        return LoginScreen()

if __name__ == '__main__':
    Storganizer().run()


import json
import urllib.request
@staticmethod
def get_obj(barcode, api_key, args=None):
    #Lookup barcode and return json object
    try:
        with (urllib.request.urlopen("https://api.barcodelookup.com/v2/products?"
        "barcode={}&formatted=y&key={}{}".format(barcode, api_key, args))) as response:
            # parse the response, returning a python object
            return json.load(response)
    except:
        print("Unable to update product barcode.\n"
            "Please check internet connection.\n",
            "If internet is not the problem, contact your database administrator.")
    return None