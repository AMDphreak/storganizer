import urllib.request
import json
import sys

# API keys:
# Ryan: 1om2sldpu9y2vjpoyumym40rqh597w
api_key = "1om2sldpu9y2vjpoyumym40rqh597w"

barcode = 890039001419
#barcode = input("barcode:")

# Lookup by barcode
# Fully formed example URL
# https://api.barcodelookup.com/v2/products?barcode=890039001419&formatted=y&key=1om2sldpu9y2vjpoyumym40rqh597w
url = "https://api.barcodelookup.com/v2/products?barcode={0}&formatted=y&key={1}".format(barcode,api_key)
#url = "https://www.google.com"

with urllib.request.urlopen(url) as response:
    #the_page = response.read()
    #print("printing page",the_page)
    with open("output.json","w+") as newfile:
        print("loading json")
        obj = json.load(response) # parses the response, creating a python object
        print("dumping json")
        json.dump(obj,newfile,indent="    ") # re-encodes the python object into a json string and writes file
        newfile.seek(0) # goes back to the beginning of the file
        print(newfile.read())